# Wilson's Active Speaker
This project was primarily created to make it easy to tell who is talking with a slight glance at the screen. 

This project acts as a demo of how to implement active speaking in a visual novel, while attempting to reduce the amount of code required to handle rendering scenes using the active speaker concept.

Another minor goal is to accomplish this while keeping the number of required images low.

## How is it done?
The person talking is rendered normally while the inactive characters are rendered with a dimmer. This is done to bring focus to the speaker and make it clear who is talking without having to look at the name of the character talking in the dialog window.

## Technical Tasks
This project uses specific features from Ren'Py, and has examples of the following:
* [Creator-Defined Statements](https://www.renpy.org/doc/html/cds.html)
* [Movement based transforms](https://www.renpy.org/doc/html/transforms.html)
* [AlphaMask](https://www.renpy.org/doc/html/displayables.html#AlphaMask)
* [Basic Composite](https://www.renpy.org/doc/html/displayables.html#Composite)
* [Image Flip](https://www.renpy.org/doc/html/displayables.html#im.Flip)
* [ConditionSwitch](https://www.renpy.org/doc/html/displayables.html#ConditionSwitch)

## Prerequisites
You will need to have the following software installed:
* [Ren'Py](https://www.renpy.org/latest.html) for running the demo. ( >=7.3.4)

## Getting Started
This project can be started with Ren'Py.

## Assets
I'm using assets from generous artists and musicians in the Ren'Py community, please support them, if you can.
### Art
* [Wisteria Sprite](https://liah0227.itch.io/wisteria) by [Liah](https://liah0227.itch.io/)
* [Hoshiko Sprite](https://liah0227.itch.io/hoshiko) by [Liah](https://liah0227.itch.io/)
* [Saki Sprite](https://liah0227.itch.io/saki) by [Liah](https://liah0227.itch.io/)
* [Megalopolitan Education Background](https://lemmasoft.renai.us/forums/viewtopic.php?f=52&t=33202&p=413452&hilit=id%3D38940#p413452) by [Uncle Mugen](https://alte.itch.io/uncle-mugens-backgrounds)
### Music
* [As the Wind Carries](https://kinetiklee.bandcamp.com/track/as-the-wind-carries) by [Kinetik Lee](https://soundcloud.com/kinetik-lee)

## Demo Video
If you want to see what this looks like when you run it without pulling it down, here is a video of the whole scene:

[Active Speaker](https://youtu.be/9DVYmCpjSX0)