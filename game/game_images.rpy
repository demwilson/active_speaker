image silvia_mask = AlphaMask("the_darkness_70", "silvia_school_smile.png")
image silvia_inactive = Composite(
    (730,1080),
    (0,0), "silvia_school_smile.png",
    (0,0), "silvia_mask"
)
image silvia_flip = im.Flip("silvia_school_smile.png", horizontal=True)
image silvia_flip_mask = AlphaMask("the_darkness_70", "silvia_flip")
image silvia_flip_inactive = Composite(
    (730,1080),
    (0,0), "silvia_flip",
    (0,0), "silvia_flip_mask"
)
image silvia = ConditionSwitch(
    "(scene_manager.active_speaker == s.name or scene_manager.ignore_speaking_flag) and s.flip_flag == True", "silvia_flip",
    "s.flip_flag == True", "silvia_flip_inactive",
    "scene_manager.active_speaker == s.name or scene_manager.ignore_speaking_flag", "silvia_school_smile.png",
    "True", "silvia_inactive")

image mari_mask = AlphaMask("the_darkness_70", "mari_school_smile.png")
image mari_inactive = Composite(
    (720,1080),
    (0,0), "mari_school_smile.png",
    (0,0), "mari_mask"
)
image mari_flip = im.Flip("mari_school_smile.png", horizontal=True)
image mari_flip_mask = AlphaMask("the_darkness_70", "mari_flip")
image mari_flip_inactive = Composite(
    (720,1080),
    (0,0), "mari_flip",
    (0,0), "mari_flip_mask"
)
image mari = ConditionSwitch(
    "(scene_manager.active_speaker == m.name or scene_manager.ignore_speaking_flag) and m.flip_flag == True", "mari_flip",
    "m.flip_flag == True", "mari_flip_inactive",
    "scene_manager.active_speaker == m.name or scene_manager.ignore_speaking_flag", "mari_school_smile.png",
    "True", "mari_inactive")

image claire_mask = AlphaMask("the_darkness_70", "claire_school_smile.png")
image claire_inactive = Composite(
    (699,1080),
    (0,0), "claire_school_smile.png",
    (0,0), "claire_mask"
)
image claire_flip = im.Flip("claire_school_smile.png", horizontal=True)
image claire_flip_mask = AlphaMask("the_darkness_70", "claire_flip")
image claire_flip_inactive = Composite(
    (699,1080),
    (0,0), "claire_flip",
    (0,0), "claire_flip_mask"
)
image claire = ConditionSwitch(
    "(scene_manager.active_speaker == s.name or scene_manager.ignore_speaking_flag) and c.flip_flag == True", "claire_flip",
    "c.flip_flag == True", "claire_flip_inactive",
    "scene_manager.active_speaker == c.name or scene_manager.ignore_speaking_flag", "claire_school_smile.png",
    "True", "claire_inactive")

