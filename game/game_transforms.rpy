transform walk_in_from_left:
     offscreenleft
     easein 1.2 left
transform walk_in_from_right:
     offscreenright
     easein 1.2 right
transform walk_off_right_from_right:
     right
     easein 4.5 offscreenright
transform walk_off_right_from_right_fast:
     right
     easein 0.6 offscreenright
transform walk_off_right_from_left:
     left
     easein 6.0 offscreenright
