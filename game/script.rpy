﻿init python:
    from wilson.scene_manager import SceneManager
    config.developer = True
    # config.debug = True
    config.log = 'active_speaker_logs.txt'
    scene_manager = SceneManager(renpy)

define s = Character("Silvia")
define c = Character("Claire")
define m = Character("Mari")

label start:
    python:
        scene_manager.__init__(renpy)
        s.flip_flag = False
        c.flip_flag = False
        m.flip_flag = False
    jump campus
    return